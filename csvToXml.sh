#!/bin/bash
file_in="simple.csv"
file_out="simple.xml"
echo '<?xml version="1.0"?>' > $file_out
echo '<members>' >> $file_out
while IFS=$'|' read -r -a arry
do
  echo '  <member><firstname>'${arry[0]}'</firstname><lastname>'${arry[1]}'</lastname><date-of-birth>'${arry[2]}'</date-of-birth></member>' >> $file_out
done < $file_in
echo '</members>' >> $file_out