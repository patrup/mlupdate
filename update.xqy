xquery version "1.0-ml";
(: get total numbers of update records, all updates are of URI: /update/* :)
declare variable $total := xdmp:estimate(xdmp:directory("/update/", "1"));
declare function local:compareAndUpdate($src,$trgt, $cnt as xs:int, $mx as xs:int)
{
    (: compare phone numbers:)
    if ($trgt//phone/text() = $src//phone/text())
    (: continue :)
    then (local:getTitleAndCallUpdateRecord($cnt, $mx))
    (: update phonenumber:)
    else (
      xdmp:node-replace($trgt//phone, $src//phone),
      xdmp:pretty-print("replaced"),
      local:getTitleAndCallUpdateRecord($cnt, $mx)
    )
};
(: helper to print output to console :)
declare function local:print($str as xs:string) {
xdmp:pretty-print($str)
};
(: create new record :)
declare function local:createRecord($rcrd, $tFT as xs:string, $cnt as xs:int, $mx as xs:int) {
(: TODO: create new record and continue :)
(xdmp:document-insert($eFT, $rcrd),
local:getTitleAndCallUpdateRecord($cnt, $mx))
};
declare function local:getTitleAndCallUpdateRecord($i as xs:int, $max as xs:int) {
  let $j := $i+1
  (: get one document uri :)
  let $updateFileTitle := cts:uri-match("/update/*")[$i]
  (: get an update file :)
  let $sourceFile := doc($updateFileTitle)
  (: get an update candidate :)
  let $targetFileTitle := fn:concat("/person",fn:substring-after($updateFileTitle, "/update")) 
  let $targetFile := doc($targetFileTitle)
  (: if update candidate exists :)
  return if ($targetFile)
    then (local:compareAndUpdate($sourceFile, $targetFile, $j, $max))
    (: if no update candidate exists, create a new record from update file :)
    else if ($sourceFile)
      then (local:createRecord($sourceFile, $targetFileTitle, $j, $max))
      else local:print("end")
  };
local:getTitleAndCallUpdateRecord(1, $total)