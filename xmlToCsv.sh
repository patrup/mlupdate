#!/usr/bin/env python

import csv
from xmlutils.xml2csv import xml2csv

inputs = "update-file.xml"
output = "update-file.csv"

converter = xml2csv(inputs, output)
converter.convert(tag="member")